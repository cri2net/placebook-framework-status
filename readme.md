
#set up

composer require "placebook/framework-status"

В контроллер api добавить в RootQueryType новый query. Например:

```php
<?php

use Placebook\Framework\Core\Schema\SystemStatusField;
use Youshido\GraphQL\Type\Object\ObjectType;

$rootQueryType = new ObjectType([
    'name' => 'RootQueryType',
    'fields' => [
        new SystemStatusField(),
    ]
]);
```

#usage

```php
<?php

$res = Api::rawRequest('{ systemStatus(show_database: true) { timestamp, status } }');
$res = $res->data->systemStatus;
$res->status = json_decode($res->status);
print_r($res->status);

?>
```
