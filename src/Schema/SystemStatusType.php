<?php
namespace Placebook\Framework\Core\Schema;

use Youshido\GraphQL\Type\Object\AbstractObjectType;
use Youshido\GraphQL\Type\Scalar\BooleanType;
use Youshido\GraphQL\Type\Scalar\FloatType;
use Youshido\GraphQL\Type\Scalar\IntType;
use Youshido\GraphQL\Type\Scalar\StringType;

/**
 * Тип данных: файл на CDN
 */
class SystemStatusType extends AbstractObjectType
{
    /**
     * implementing an abstract function where you build your type
     */
    public function build($config)
    {
        $config
            ->addField('timestamp', new FloatType())
            ->addField('status',    new StringType());
    }
}
