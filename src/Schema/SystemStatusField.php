<?php
namespace Placebook\Framework\Core\Schema;

use Youshido\GraphQL\Field\AbstractField;
use Youshido\GraphQL\Execution\ResolveInfo;
use Youshido\GraphQL\Config\Field\FieldConfig;
use Youshido\GraphQL\Type\NonNullType;
use Youshido\GraphQL\Type\Scalar\BooleanType;
use Youshido\GraphQL\Type\Scalar\IntType;
use Youshido\GraphQL\Type\Scalar\StringType;

use \Placebook\Framework\Core\ApiTokens;
use \cri2net\php_pdo_db\PDO_DB;

use \Linfo\Exceptions\FatalException;
use \Linfo\Linfo;

/**
 * Получение текущей информации о системе
 */
class SystemStatusField extends AbstractField
{
    public function getType()
    {
        return new SystemStatusType();
    }

    public function build(FieldConfig $config)
    {
        $config->addArgument('show_database', new BooleanType());
    }

    public static function isUtf8($string)
    {
        return ($string === mb_convert_encoding(mb_convert_encoding($string, "UTF-32", "UTF-8"), "UTF-8", "UTF-32"));
    }

    public function resolve($value, array $args, ResolveInfo $info)
    {
        if (class_exists('\Placebook\Framework\Core\ApiTokens')) {
            $permission = ApiTokens::checkPermis($this->getName(), 'query');
        } else {
            $permission = ['is_full' => true];
        }

        $result = [
            'timestamp' => microtime(true),
            'status'    => [],
        ];

        if (isset($args['show_database'])) {

            $stm = PDO_DB::query("SHOW STATUS");
            $result['status']['database'][0] = [];

            while ($row = $stm->fetch()) {
                $result['status']['database'][0][$row['Variable_name']] = $row['Value'];
            }
        }

        $result['status']['system'] = [];

        try {
            
            $linfo = new Linfo();
            $parser = @$linfo->getParser();
            $keys = ['Os', 'Kernel', 'HostName', 'Ram', 'CPU', 'UpTime', 'Mounts', 'Load', 'Net', 'ProcessStats', 'Services', 'CPUArchitecture'];

            foreach ($keys as $key) {

                try {
                    $method = 'get' . $key;
                    $result['status']['system'][$key] = @$parser->$method();
                } catch (FatalException $e) {
                }
            }

            if (!self::isUtf8($result['status']['system']['Os'])) {
                $result['status']['system']['Os'] = iconv('CP1251', 'UTF-8', $result['status']['system']['Os']);
            }
            
        } catch (FatalException $e) {
        }

        $result['status'] = json_encode($result['status']);

        if (!$permission['is_full']) {
            foreach ($result as $key => $item) {
                $result[$key] = ApiTokens::unsetFieldsWithoutPermission($item, $permission['fields']);
            }
        }
        
        return $result;
    }
}
